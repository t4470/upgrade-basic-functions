// Iteración #6: Valores únicos (medio copiado )
const duplicates = ['sushi','pizza','burger','potatoe','pasta','ice-cream','pizza','chicken','onion rings','pasta','soda'];

function removeDuplicates(array){
    var uniqueArray = [];
    for(i=0; i < array.length; i++){
      if(uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }
  console.log(removeDuplicates(duplicates));