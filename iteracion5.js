// NO ME SALE //
// Iteración #5: Calcular promedio de strings
// Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y de lo contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:
const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];

let sum = 0;
let sumNumbers = 0;
let sumString = 0;

function averageWord(arr) {
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === "number") {
            sumNumbers += arr[i];
        }
        if (typeof arr[i] === "string") {
            sumString += arr[i].length;
        }
        sum = sumNumbers + sumString;
    }
    return sum;
}
console.log(averageWord(mixedElements));
console.log(sumString, sumNumbers)