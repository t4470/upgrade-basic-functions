// Iteration #8: Contador de repeticiones
// Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma.  
// Puedes usar este array para probar tu función:
const counterWords = ['code', 'repeat', 'eat', 'sleep', 'code', 'enjoy', 'sleep', 'code', 'enjoy', 'upgrade', 'code'];
function contadorRepeat(array) {
    const repeatedWords = {}
    array.forEach(item => {
        if (repeatedWords[item]) {
            repeatedWords[item] += 1
            return
        }
        repeatedWords[item] = 1
    })
    console.log(repeatedWords);

}
contadorRepeat(counterWords);