// Iteración #7: Buscador de nombres
// existe el elemento, en caso que existan nos devuelve un true y la posición de dicho elemento y por la contra un false. Puedes usar este array para probar tu función:
const nameFinder = ['Peter','Steve','Tony','Natasha','Clint','Logan','Xabier','Bruce','Peggy','Jessica','Marc'];

function finderName(arr, element, index) {
		let isInArray = arr.includes(element)
		if(isInArray) {
			return true + ' Estoy en posicion ' + arr.indexOf(element, index);
		} else {
			return false;
	}
}
console.log(finderName(nameFinder, 'Peter')) // true posicion 0 
console.log(finderName(nameFinder, 'Tony')) // true posicion 2 
console.log(finderName(nameFinder, 'Elias')) // false