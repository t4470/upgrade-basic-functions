// Iteración #2: Buscar la palabra más larga
const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
let longerWord = '';

function findLongestWord(array) {
    array.forEach(function (element) {
        if (longerWord.length < element.length) {
            longerWord = element;
        }
    });
    return longerWord;
}
console.log(findLongestWord(avengers));

// otra manera
/* const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
function findLongestWord(avengers) {
    for (var index = 0; index < avengers.length; index++) {
        orderByLength = avengers.sort((a, b) => b.length - a.length);
    }
    return avengers[0];
}
findLongestWord(avengers); */